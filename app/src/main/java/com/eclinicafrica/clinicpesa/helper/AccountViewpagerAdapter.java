package com.eclinicafrica.clinicpesa.helper;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;

/**
 * Created by bryanlamtoo on 20/04/2017.
 */

public class AccountViewpagerAdapter extends PagerAdapter {

    private Context mContext;

    @Override
    public int getCount() {
        return 3;
    }

    public AccountViewpagerAdapter(Context context){
        this.mContext = context;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
