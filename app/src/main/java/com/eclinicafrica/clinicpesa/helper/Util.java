package com.eclinicafrica.clinicpesa.helper;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.widget.Toast;

/**
 * Created by bryanlamtoo on 20/04/2017.
 */

public class Util {

    public static void showToast(final Activity activity, final String message) {

        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Converting dp to pixel
     */
    public static int dpToPx(Context context,int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
