package com.eclinicafrica.clinicpesa.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceFragment;

import com.eclinicafrica.clinicpesa.models.UserInfo;

import java.util.HashMap;

/**
 * Created by bryanlamtoo on 14/04/2017.
 */

public class PrefManager extends PreferenceFragment {

//    private UserInfoDao userInfoDao;

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "clinicPESA";

    // All Shared Preferences Keys
    private static final String KEY_IS_WAITING_FOR_SMS = "IsWaitingForSms";
    private static final String KEY_MOBILE_NUMBER = "mobile_number";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";

    public PrefManager() {
    }

    public void setIsWaitingForSms(boolean isWaiting) {
        editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting);
        editor.commit();
    }

    public boolean isWaitingForSms() {
        return pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false);
    }

    public void setMobileNumber(String mobileNumber) {
        editor.putString(KEY_MOBILE_NUMBER, mobileNumber);
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(KEY_MOBILE_NUMBER, null);
    }

    public void createLogin(String name, String email, String mobile) {
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("email", pref.getString(KEY_EMAIL, null));
        profile.put("mobile", pref.getString(KEY_MOBILE, null));
        return profile;
    }

    public static PrefManager newInstance(Context context) {
        PrefManager fragment = new PrefManager();
        fragment._context = context;
        fragment.pref = fragment._context.getSharedPreferences(PREF_NAME, fragment.PRIVATE_MODE);
//        fragment.userInfoDao = ((ApplicationLoader) fragment._context.getApplication()).getDaoSession().getUserInfoDao();
        fragment.editor = fragment.pref.edit();
        return fragment;
    }

    public UserInfo getuserInfo() {
        return new UserInfo();
    }
}
