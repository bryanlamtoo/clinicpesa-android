package com.eclinicafrica.clinicpesa.helper;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Bryan Lamtoo - creativeDNA (U) LTD on 18/12/2015.
 */
public class ClinicPesaViewPager extends ViewPager {

    public ClinicPesaViewPager(Context context) {
        super(context);
    }

    public ClinicPesaViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }
}
