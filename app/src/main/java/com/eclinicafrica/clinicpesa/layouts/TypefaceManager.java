package com.eclinicafrica.clinicpesa.layouts;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.util.LruCache;

/**
 * Created by Bryan Lamtoo - creativeDNA (U) LTD on 29/01/16.
 */
public class TypefaceManager {

    private static final String ROBOTO_LIGHT_FILENAME = "Roboto-Light.ttf";
    private static final String ROBOTO_CONDENSED_FILENAME = "RobotoCondensed-Regular.ttf";
    private static final String ROBOTO_CONDENSED_BOLD_FILENAME = "RobotoCondensed-Bold.ttf";
    private static final String ROBOTO_CONDENSED_LIGHT_FILENAME = "RobotoCondensed-Light.ttf";
    private static final String ROBOTO_SLAB_FILENAME = "RobotoSlab-Regular.ttf";
    private static final String ROBOTO_ITALICS_FILENAME = "Roboto-MediumItalic.ttf";
    private static final String ROBOTO_RUGULAR_FILENAME = "Roboto-Regular.ttf";
    private static final String ROBOTO_BOLD_FILENAME = "Roboto-Bold.ttf";
    private static final String ROBOTO_THIN_FILENAME = "Roboto-Thin.ttf";
    private static final String FUTURA_FILENAME = "Futura.ttf";

    private static final String ROBOTO_LIGHT_NATIVE_FONT_FAMILY = "sans-serif-light";
    private static final String ROBOTO_CONDENSED_NATIVE_FONT_FAMILY = "sans-serif-condensed";

    private final LruCache<String, Typeface> mCache;
//    private final AssetManager mAssetManager;

    private Context mContext;
    public TypefaceManager(Context context) {
        this.mContext =context;
//        mAssetManager = new AssetManager();
//        mAssetManager = Preconditions.checkNotNull(assetManager, "assetManager cannot be null");
        mCache = new LruCache<>(3);
    }

    public Typeface getRobotoLight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return Typeface.create(ROBOTO_LIGHT_NATIVE_FONT_FAMILY, Typeface.NORMAL);
        }
        return getTypeface(ROBOTO_LIGHT_FILENAME);
    }

    public Typeface getRobotoCondensed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return Typeface.create(ROBOTO_CONDENSED_NATIVE_FONT_FAMILY, Typeface.NORMAL);
        }
        return getTypeface(ROBOTO_CONDENSED_FILENAME);
    }

    public Typeface getRobotoCondensedBold() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return Typeface.create(ROBOTO_CONDENSED_NATIVE_FONT_FAMILY, Typeface.BOLD);
        }
        return getTypeface(ROBOTO_CONDENSED_BOLD_FILENAME);
    }

    public Typeface getRobotoCondensedLight() {
        return getTypeface(ROBOTO_CONDENSED_LIGHT_FILENAME);
    }

    public Typeface getRobotoItalics(){
        return getTypeface(ROBOTO_ITALICS_FILENAME);
    }

    public Typeface getRobotoSlab() {
        return getTypeface(ROBOTO_SLAB_FILENAME);
    }

    public Typeface getRobotoRegular() {
        return getTypeface(ROBOTO_RUGULAR_FILENAME);
    }

    public Typeface getRobotoBold() {
        return getTypeface(ROBOTO_BOLD_FILENAME);
    }

    public Typeface getRobotoThin(){
        return getTypeface(ROBOTO_THIN_FILENAME);
    }
    public Typeface getFutura(){
        return getTypeface(FUTURA_FILENAME);
    }

    private Typeface getTypeface(final String filename) {
        Typeface typeface = mCache.get(filename);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/" + filename);
            mCache.put(filename, typeface);
        }
        return typeface;
    }
}
