package com.eclinicafrica.clinicpesa.layouts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.eclinicafrica.clinicpesa.R;


/**
 * Created by Bryan Lamtoo - creativeDNA (U) LTD on 29/01/16.
 */
public class ClinicPesaTextView extends TextView {
    public static final int FONT_ROBOTO_LIGHT = 1;
    public static final int FONT_ROBOTO_CONDENSED = 2;
    public static final int FONT_ROBOTO_CONDENSED_LIGHT = 3;
    public static final int FONT_ROBOTO_CONDENSED_BOLD = 4;
    public static final int FONT_ROBOTO_SLAB = 5;
    public static final int FONT_ROBOTO_ITALICS = 6;
    public static final int FONT_ROBOTO_REGULAR = 7;
    public static final int FONT_ROBOTO_BOLD= 8;
    public static final int FONT_ROBOTO_THIN= 9;
    public static final int FONT_FUTURA= 10;

    TypefaceManager mTypefaceManager;


    public ClinicPesaTextView(Context context) {
        this(context, null);
    }

    public ClinicPesaTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ClinicPesaTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mTypefaceManager = new TypefaceManager(context);

        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ClinicPesaTextView);
            setFont(a.getInt(R.styleable.ClinicPesaTextView_font, 0));
            a.recycle();
        }
    }

    public void setFont(final int customFont) {
        Typeface typeface = getFont(mTypefaceManager, customFont);
        if (typeface != null) {
            setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
            setTypeface(typeface);
        }
    }

    public static Typeface getFont(TypefaceManager typefaceManager, final int customFont) {
        Typeface typeface = null;

        switch (customFont) {
            case FONT_ROBOTO_LIGHT:
                typeface = typefaceManager.getRobotoLight();
                break;
            case FONT_ROBOTO_CONDENSED:
                typeface = typefaceManager.getRobotoCondensed();
                break;
            case FONT_ROBOTO_CONDENSED_LIGHT:
                typeface = typefaceManager.getRobotoCondensedLight();
                break;
            case FONT_ROBOTO_CONDENSED_BOLD:
                typeface = typefaceManager.getRobotoCondensedBold();
                break;
            case FONT_ROBOTO_SLAB:
                typeface = typefaceManager.getRobotoSlab();
                break;
            case FONT_ROBOTO_ITALICS:
                typeface = typefaceManager.getRobotoItalics();
                break;
            case FONT_ROBOTO_REGULAR:
                typeface = typefaceManager.getRobotoRegular();
                break;

            case FONT_ROBOTO_BOLD:
                typeface = typefaceManager.getRobotoBold();
                break;

            case FONT_ROBOTO_THIN:
                typeface = typefaceManager.getRobotoThin();
                break;

            case FONT_FUTURA:
                typeface = typefaceManager.getFutura();
                break;
        }

        return typeface;
    }
}
