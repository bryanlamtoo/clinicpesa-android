package com.eclinicafrica.clinicpesa.models;

/**
 * Created by bryanlamtoo on 23/04/2017.
 */

public class HomeScreenItem {

    int titleResId;
    int iconResId;

    public int getTitleResId() {
        return titleResId;
    }

    public void setTitleResId(int titleResId) {
        this.titleResId = titleResId;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public HomeScreenItem(int titleResId, int iconResId) {
        this.titleResId = titleResId;
        this.iconResId = iconResId;
    }
}
