package com.eclinicafrica.clinicpesa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by bryanlamtoo on 23/04/2017.
 */

//@Entity
public class UserInfo {

//    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("admin")
    @Expose
    private String admin;
    @SerializedName("is_verified")
    @Expose
    private boolean isVerified;
    @SerializedName("walletbalance")
    @Expose
    private int walletbalance;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("mobilen")
    @Expose
    private String mobilen;
    @SerializedName("verification_code")
    @Expose
    private String verificationCode;
    @SerializedName("is_mobile_verified")
    @Expose
    private boolean isMobileVerified;
    @SerializedName("walletpin")
    @Expose
    private int walletpin;
    @SerializedName("auth_token")
    @Expose
    private String authToken;
    @SerializedName("avatar")
    @Expose
    private String avatar;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public boolean isIsVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public int getWalletbalance() {
        return walletbalance;
    }

    public void setWalletbalance(int walletbalance) {
        this.walletbalance = walletbalance;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMobilen() {
        return mobilen;
    }

    public void setMobilen(String mobilen) {
        this.mobilen = mobilen;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public boolean getIsMobileVerified() {
        return isMobileVerified;
    }

    public void setIsMobileVerified(boolean isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public int getWalletpin() {
        return walletpin;
    }

    public void setWalletpin(int walletpin) {
        this.walletpin = walletpin;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean getIsVerified() {
        return this.isVerified;
    }

}
