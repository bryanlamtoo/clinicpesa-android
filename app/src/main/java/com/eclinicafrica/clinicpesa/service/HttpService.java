package com.eclinicafrica.clinicpesa.service;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by bryanlamtoo on 14/04/2017.
 */

public class HttpService extends IntentService {
    private static String TAG = HttpService.class.getSimpleName();

    public HttpService() {
        super(HttpService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String otp = intent.getStringExtra("otp");
            verifyOtp(otp);
        }
    }

    private void verifyOtp(String otp) {

    }
}
