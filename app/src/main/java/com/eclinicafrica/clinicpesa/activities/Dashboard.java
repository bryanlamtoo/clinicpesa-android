package com.eclinicafrica.clinicpesa.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.eclinicafrica.clinicpesa.R;
import com.eclinicafrica.clinicpesa.fragments.ProfileFragment;
import com.eclinicafrica.clinicpesa.helper.PrefManager;
import com.eclinicafrica.clinicpesa.interfaces.OnFragmentInteractionListener;
import com.eclinicafrica.clinicpesa.models.UserInfo;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Dashboard extends AppCompatActivity implements OnFragmentInteractionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private int backPressed;


    private AccountHeader headerResult = null;
    private Drawer result = null;

    private IProfile profile;

    private static final int MANAGE_ACCOUNT = 1;

    //    @BindView(R.id.bottomBar)
//    BottomBar bottomBar;
    private List<Fragment> activeFragments = new ArrayList<>();

    @BindView(R.id.account_balance_viewgroup)
    RelativeLayout appBarView;

    @BindView(R.id.contentContainer)
    FrameLayout container;

    @BindView(R.id.account_balance)
    TextView mainAccountBalance;
    private UserInfo userInfo;
    private MaterialDialog progressDialog;
    private PrefManager prefs;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

//        Toast.makeText(this, "Called", Toast.LENGTH_SHORT).show();

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            appBarView.setVisibility(View.GONE);
            getSupportActionBar().setTitle("clinicPesa");

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            appBarView.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle("");


        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        setUpDashboard(savedInstanceState);

//        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
//            @Override
//            public void onTabSelected(@IdRes int tabId) {
//                if (tabId == R.id.tab_home) {
//                    // The tab with id R.id.home was selected,
//                    // change your content accordingly.
//
//                    HomeFragment transactionsFragment = new HomeFragment();
//                    getSupportFragmentManager()
//                            .beginTransaction()
//                            .replace(R.id.contentContainer, transactionsFragment)
//                            .commit();
//
//                    activeFragments.add(transactionsFragment);
//                }
//            }
//        });
    }

    private void setUpDashboard(Bundle savedInstanceState) {
        prefs = PrefManager.newInstance(this);
        userInfo = prefs.getuserInfo();

        DecimalFormat formatter = new DecimalFormat("#,###,###");
//        mainAccountBalance.setText(formatter.format(userInfo.getWalletbalance()));

        progressDialog = new MaterialDialog.Builder(this)
                .title("Initiating deposit")
                .theme(Theme.LIGHT)
                .content("Please wait")
                .autoDismiss(false)
                .progress(true, 0)
                .build();

        profile = new ProfileDrawerItem().
                withName("Bryan Lamtoo")
                .withEmail("bryanlamtoo@gmail.com");

        if (null != userInfo.getAvatar() && !userInfo.getAvatar().isEmpty()) {
            profile
                    .withIcon(userInfo.getAvatar());
        } else {
            profile.withIcon(R.drawable.profile);
        }
        // Create the AccountHeader
        buildHeader(false, savedInstanceState);

        buildDrawer(savedInstanceState);
    }


    private void buildHeader(boolean compact, Bundle savedInstanceState) {
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorPrimary)
                .withCompactStyle(compact)
                .addProfiles(
                        profile,
                        //don't ask but google uses 14dp for the add account icon in gmail but 20dp for the normal icons (like manage account)
                        new ProfileSettingDrawerItem().withName("Manage Account").withIcon(GoogleMaterial.Icon.gmd_settings)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {


                        if (profile instanceof IDrawerItem && profile.getIdentifier() == MANAGE_ACCOUNT) {

//                            bottomBar.selectTabWithId(R.id.tab_profile);

                        }

                        //false if you have not consumed the event and it should close the drawer
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    private void buildDrawer(Bundle savedInstanceState) {
        //Create the drawer
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_home).withIcon(FontAwesome.Icon.faw_home).withBadgeStyle
                                (new BadgeStyle().withColorRes(R.color.md_green_600).withTextColorRes(R.color.md_white_1000)).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_deposit_money).withIcon(FontAwesome.Icon.faw_google_wallet).withIdentifier(7),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_facilities).withIcon(GoogleMaterial.Icon.gmd_local_hotel).withIdentifier(4).withEnabled(false),
                        new PrimaryDrawerItem().withName(R.string.search_pay).withIcon(FontAwesome.Icon.faw_search).withIdentifier(10).withEnabled(true),
//                        new PrimaryDrawerItem().withName(R.string.drawer_item_wallet).withIcon(FontAwesome.Icon.faw_google_wallet).withIdentifier(6),
                        new SectionDrawerItem().withName(R.string.drawer_item_section_header),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_help).withIcon(FontAwesome.Icon.faw_database).withIdentifier(2),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_about).withIcon(FontAwesome.Icon.faw_info).withIdentifier(9),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(11)
//                        new SecondaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(8)

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D

                        if (drawerItem != null) {
                            Intent intent = null;

                            if (drawerItem.getIdentifier() == 1) {
                                removeAllActiveFragments();
                            } else if (drawerItem.getIdentifier() == 2) {

                                String url = "https://clinicpesa.com/faqs";
                                intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(url));


                            } else if (drawerItem.getIdentifier() == 3) {
//                                SendToMobileFragment sendToMobileFragment = new SendToMobileFragment();
//                                getSupportFragmentManager()
//                                        .beginTransaction()
//                                        .replace(R.id.container, sendToMobileFragment)
//                                        .commit();

//                                activeFragments.add(sendToMobileFragment);

                            } else if (drawerItem.getIdentifier() == 4) {

                            } else if (drawerItem.getIdentifier() == 5) {

//                                VerificationsFragment verificationsFragment = new VerificationsFragment();
//                                getSupportFragmentManager()
//                                        .beginTransaction()
//                                        .replace(R.id.container, verificationsFragment)
//                                        .commit();

//                                activeFragments.add(verificationsFragment);


                            } else if (drawerItem.getIdentifier() == 7) {

//                                requestMoneyDialog.show();

                            } else if (drawerItem.getIdentifier() == 8) {


                            } else if (drawerItem.getIdentifier() == 9) {

                                String url = "https://clinicpesa.com/about%20us";
                                intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(url));

                            } else if (drawerItem.getIdentifier() == 10) {
//                                SearchAndPayFragment searchAndPayFragment = new SearchAndPayFragment();
//                                getSupportFragmentManager()
//                                        .beginTransaction()
//                                        .replace(R.id.container, searchAndPayFragment).commit();
//
//                                activeFragments.add(searchAndPayFragment);
                            } else if (drawerItem.getIdentifier() == 20) {

                                new MaterialDialog.Builder(Dashboard.this)
                                        .title("Sign Out")
                                        .backgroundColorRes(R.color.colorPrimaryDark)
                                        .content("Are you sure you want to Sign out?")
                                        .positiveText("Yes")
                                        .negativeText("Cancel")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


//                                                logoutUser();

                                            }
                                        })
                                        .show();
                            }


                            if (intent != null) {
                                Dashboard.this.startActivity(intent);
                            }

                        }


                        return false;
                    }
                })
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
                        Dashboard.this.finish();
                        //return true if we have consumed the event
                        return true;
                    }
                })
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.drawer_item_sign_out).withIcon(FontAwesome.Icon.faw_sign_out).withIdentifier(20)
                )

                .withSavedInstance(savedInstanceState)
                .build();

//        updateHomeItemBadge();

        // set the selection to the item with the identifier 11
        if (savedInstanceState != null)
            result.setSelection(1, false);
    }


    @Override
    public void onBackButtonClicked() {
        removeAllActiveFragments();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            backPressed++;
            removeAllActiveFragments();

            if (backPressed == 1) {

                Toast.makeText(Dashboard.this, "Press again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        backPressed = 0;
                    }
                }, 10000);
            } else if (backPressed == 2) {
                backPressed = 0;
                finish();
            }

        }
    }

    private void removeAllActiveFragments() {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (activeFragments.size() > 0) {
            for (int i = 0; i < activeFragments.size(); i++) {
                ft.remove(activeFragments.get(i));
            }
            activeFragments.clear();
            ft.commit();
        }

    }

    @Override
    public void onMyAccountSelected() {
//        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();

        ProfileFragment profileFragment = new ProfileFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contentContainer, profileFragment)
                .commit();
        activeFragments.add(profileFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
