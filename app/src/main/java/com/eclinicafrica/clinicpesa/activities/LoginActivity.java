package com.eclinicafrica.clinicpesa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.eclinicafrica.clinicpesa.R;

public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void onSignInButtonClicked(View view) {

    }

    public void onPassWordForgotten(View view) {
    }

    public void onGoToSignUpClicked(View view) {
        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));

    }
}
