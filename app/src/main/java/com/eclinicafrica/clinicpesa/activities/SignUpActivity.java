package com.eclinicafrica.clinicpesa.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.eclinicafrica.clinicpesa.R;
import com.eclinicafrica.clinicpesa.country.CountryCodePicker;
import com.eclinicafrica.clinicpesa.helper.PrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.eclinicafrica.clinicpesa.helper.Util.showToast;

public class SignUpActivity extends AppCompatActivity {

    private static final int MIN_CODE_SIZE = 4;
    @BindView(R.id.layout_edit_mobile)
    LinearLayout layoutEditMobile;
    @BindView(R.id.viewPagerVertical)
    ViewPager viewPager;

    @BindView(R.id.account_viewpager)
    ViewPager accountViewpager;
    private PrefManager pref;


    private ViewPagerAdapter adapter;
    private AccountTypeAdapter accountAdapter;
    @BindView(R.id.activity_sign_up)
    View rootView;
    MaterialDialog accountTypeSelectorDialog = null;
    private int selectedAccountTypePosition = 0;
    @BindView(R.id.accountType_tv)
    TextView accountTypePrefix_tv;

    @BindView(R.id.country_code)
    CountryCodePicker ccp;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private EditText phoneNumber_et;

    @BindView(R.id.sim_number)
            TextView simNumber;

    String prefix;
    String phoneNumber;
    private MaterialDialog progressDialog;

    @OnClick(R.id.account_type_spinner)
    public void showAccountTypeSelectionDialog() {
        accountTypeSelectorDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Create Account");

        setUpRetrofit();

        initViews();


    }

    private void setUpRetrofit() {
        pref = PrefManager.newInstance(this);


    }

    private void initViews() {
        progressDialog = new MaterialDialog.Builder(this)
                .theme(Theme.LIGHT)
                .title(R.string.creating_account)
                .content(R.string.please_wait)
                .autoDismiss(false)
                .progress(true, 0)
                .build();

        initialiseLayoutOne();

        accountTypeSelectorDialog = new MaterialDialog.Builder(this)
                .title("Choose Account Type")
                .theme(Theme.LIGHT)
                .items(R.array.account_types)
                .itemsCallbackSingleChoice(selectedAccountTypePosition, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        selectedAccountTypePosition = which;
                        accountTypePrefix_tv.setText(text);
                        accountTypeSelectorDialog.setSelectedIndex(selectedAccountTypePosition);
                        accountViewpager.setCurrentItem(selectedAccountTypePosition);


                        /**
                         * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected radio button to actually be selected.
                         **/
                        return true;
                    }
                })
                .build();


        // hiding the edit mobile number
        layoutEditMobile.setVisibility(View.GONE);
        adapter = new ViewPagerAdapter(this);
        accountAdapter = new AccountTypeAdapter(this);

        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(0);

        accountViewpager.setAdapter(accountAdapter);
        accountViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        accountViewpager.setCurrentItem(0);

    }


    public void onGoToLoginClicked(View view) {
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        finish();
    }

    public void registerNewUserAccount(View view) {

        startActivity(new Intent(SignUpActivity.this,Dashboard.class));

    }

    class AccountTypeAdapter extends PagerAdapter {

        Context mContext;

        public AccountTypeAdapter(Context context) {
            mContext = context;
        }


        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            int resId = 0;
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout;
            switch (position) {
                case 0:
                    resId = R.layout.included_create_account_form_user;
                    break;
                case 1:
                    resId = R.layout.included_create_account_form_business;
                    break;
                case 2:
                    resId = R.layout.included_create_account_form_clinic;
                    break;
            }
            layout = (ViewGroup) inflater.inflate(resId, container, false);
            container.addView(layout);

            return layout;


        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);


        }
    }


    class ViewPagerAdapter extends PagerAdapter {

        Context mContext;

        public ViewPagerAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }


        public Object instantiateItem(View collection, int position) {

            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.layout_sms;
                    break;
                case 1:
                    resId = R.id.layout_otp;
                    break;
                case 2:
                    resId = R.id.layout_signup;
                    break;
            }
            return rootView.findViewById(resId);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }


    private void initialiseLayoutOne() {
//
        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        phoneNumber_et = (EditText) findViewById(R.id.number_et);

//        askForPermission(android.Manifest.permission.READ_PHONE_STATE, READ_PHONE_NUMBER);


        phoneNumber_et.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (phoneNumber_et.getText().toString().matches("^0")) {
                    // Not allowed
                    phoneNumber_et.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });
        phoneNumber_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                // If DONE or Enter were pressed, validate the input.
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                    inputMethodManager.hideSoftInputFromWindow(phoneNumber_et.getWindowToken(), 0);
                    Editable phoneNumber = phoneNumber_et.getText();

                    if (TextUtils.isEmpty(phoneNumber.toString()) || phoneNumber.toString()
                            .length() < 5)
                        phoneNumber_et.setError(getResources().getString(R.string.error_phone_number));
                    return true;
                }
                return false;
            }
        });
        FloatingActionButton verify_number = (FloatingActionButton) findViewById(R.id.verify_phone);
        verify_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(phoneNumber_et.getWindowToken(), 0);

                final String prefix = ccp.getSelectedCountryCodeWithPlus();
                Editable phoneNumberEdit = phoneNumber_et.getText();

                if (phoneNumberEdit != null) {


                    phoneNumber = prefix + phoneNumberEdit.toString();

                    if (TextUtils.isEmpty(prefix) || TextUtils.isEmpty(phoneNumber) ||
                            phoneNumber.length() <= 11) {
                        phoneNumber_et.setError(getResources().getString(R.string.error_phone_number));

                        Animation shake = AnimationUtils.loadAnimation(SignUpActivity.this, R
                                .anim.shake);
                        phoneNumber_et.startAnimation(shake);
                        showToast(SignUpActivity.this, "Please enter valid mobile number\"");

                    } else {
                        showProgress(true, "Verifying phone number");

                        // saving the mobile number in shared preferences
                        pref.setMobileNumber(phoneNumber);

                        /**
                         * requesting for sms
                         */
                        initiateNumberVerification(phoneNumber);


                    }
                }
            }
        });

        ImageButton editNumber = (ImageButton) rootView.findViewById(R.id.btn_edit_mobile);
        editNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
                layoutEditMobile.setVisibility(View.GONE);

            }
        });

        final EditText code_et = (EditText) findViewById(R.id.code_et);
        code_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // If DONE or Enter were pressed, validate the input.
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                    inputMethodManager.hideSoftInputFromWindow(code_et.getWindowToken(), 0);
                    Editable codeEdit = code_et.getText();
                    if (TextUtils.isEmpty(codeEdit.toString()) || codeEdit.toString().length() < MIN_CODE_SIZE)
                        code_et.setError(getResources().getString(R.string.error_code));
                    return true;
                }
                return false;
            }
        });


        FloatingActionButton confirm_btn = (FloatingActionButton) findViewById(R.id.confirm_bv);
        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                inputMethodManager.hideSoftInputFromWindow(code_et.getWindowToken(), 0);
                Editable codeEdit = code_et.getText();

                if (codeEdit != null) {
                    String pinCode = codeEdit.toString();
                    if (TextUtils.isEmpty(pinCode) || pinCode.length() < MIN_CODE_SIZE)
                        code_et.setError(getResources().getString(R.string.error_code));
                    else {

                        showProgress(true, "Checking code");
                        checkPinCode(pinCode);

                    }
                }
            }
        });

    }

    private void checkPinCode(String pinCode) {


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showProgress(false, "");
                showToast(SignUpActivity.this, "Verified");
                viewPager.setCurrentItem(2);

            }
        }, 1000);

    }

    private void initiateNumberVerification(String phoneNumber) {

        // This solution will leak memory!  Don't use!!!
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(1);
                showProgress(false, "");
            }
        }, 1000);

        simNumber.setText(phoneNumber);
    }


    void showProgress(boolean show, String title) {
        progressDialog.setTitle(title);
        if (show)
            progressDialog.show();
        else
            progressDialog.hide();

    }


}
