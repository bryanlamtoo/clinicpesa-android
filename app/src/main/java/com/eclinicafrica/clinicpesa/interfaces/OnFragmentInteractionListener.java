package com.eclinicafrica.clinicpesa.interfaces;

/**
 * Created by bryanlamtoo on 02/02/2017.
 */

public interface OnFragmentInteractionListener {
    void onBackButtonClicked();

    void onMyAccountSelected();
}
