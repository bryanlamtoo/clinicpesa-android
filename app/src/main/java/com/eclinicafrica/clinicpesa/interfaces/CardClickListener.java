package com.eclinicafrica.clinicpesa.interfaces;

import android.view.View;

/**
 * Created by bryanlamtoo on 02/02/2017.
 */

public interface CardClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
