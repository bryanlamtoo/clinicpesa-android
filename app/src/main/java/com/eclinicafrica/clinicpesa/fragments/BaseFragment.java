package com.eclinicafrica.clinicpesa.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.eclinicafrica.clinicpesa.R;
import com.eclinicafrica.clinicpesa.activities.Dashboard;
import com.eclinicafrica.clinicpesa.helper.PrefManager;
import com.eclinicafrica.clinicpesa.interfaces.OnFragmentInteractionListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    protected OnFragmentInteractionListener mListener;
    protected Dashboard dashboard;
    protected PrefManager prefs;
//    protected PesamoniAPI backend;
//    protected Retrofit retrofit;
//    protected SettingsFragment pref;
//    protected UserInfo userInfo;
    private MaterialDialog progressDialog;
//    private SearchHistoryTable mHistoryDatabase;

    protected static final String EXTRA_KEY_TEXT = "text";
    private static final String EXTRA_KEY_VERSION = "version";
    private static final String EXTRA_KEY_THEME = "theme";
    private static final String EXTRA_KEY_VERSION_MARGINS = "version_margins";
//    private SearchView mSearchView;


    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpViews();
//        setUpRetrofit();

//        userInfo = pref.getuserInfo();
    }


    private void setUpViews() {
        progressDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.creating_account)
                .theme(Theme.LIGHT)
                .content(R.string.please_wait)
                .autoDismiss(false)
                .progress(true, 0)
                .build();

    }

//    private void setUpRetrofit() {
//
//        pref = SettingsFragment.newInstance(getActivity());
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY); //TODO change this to NONE
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//        Gson gson = new GsonBuilder()
//                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//                .create();
//
//        retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.API_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .client(client)
//                .build();
//
//        // Create an instance of our GitHub API interface.
//        backend = retrofit.create(PesamoniAPI.class);
//
//    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;

            dashboard = (Dashboard) context;
            prefs = PrefManager.newInstance(dashboard);

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
//        EventBus.getDefault().unregister(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mListener.onBackButtonClicked();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void showProgressDialog(boolean show, String s) {
        progressDialog.setTitle(s);

        if (show)
            progressDialog.show();
        else
            progressDialog.hide();


    }




//    @Subscribe
//    public void handleActivityResult(ActivityResultEvent event){
//
//        int requestCode = event.getRequestCode();
//        int resultCode = event.getResultCode();
//        Intent data = event.getData();
//        onActivityResult(requestCode, resultCode, data);
//    }

}
