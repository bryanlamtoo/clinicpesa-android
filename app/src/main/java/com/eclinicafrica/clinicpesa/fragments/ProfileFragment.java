package com.eclinicafrica.clinicpesa.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.eclinicafrica.clinicpesa.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pesatoolbar.PesaToolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MaterialDialog depositDialog;

    PesaToolbar pesaToolbar;
    int primary2;
    int primaryDark2;

    @BindView(R.id.layout_app_bar)
    AppBarLayout appBarLayout;

    @BindView(R.id.fab_photo)
    FloatingActionButton fabPhoto;

    @OnClick(R.id.account_topup)
    public void topUp() {
        depositDialog.show();

    }

    @OnClick(R.id.invite_someone)
    public void shareApp() {


        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/dev?id=7935326194845463814");

        try {
            startActivity(Intent.createChooser(intent, "Select an action"));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        primary2 = getResources().getColor(R.color.colorPrimary);
        primaryDark2 = getResources().getColor(R.color.colorPrimaryDark);

        disableAppBarDrag();
        hideFab();

        pesaToolbar = pesaToolbar.builder(dashboard, toolbar)
                .withToolbarAsSupportActionBar()
                .withTitle("Bryan Lamtoo")
                .withSubtitle("bryanlamtoo@gmail.com")
                .withPicture(R.drawable.profile)
                .withHidePictureWhenCollapsed(false)
                .build();

        pesaToolbar.expand();

        pesaToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pesaToolbar.isCollapsed()) {
                    pesaToolbar.expand(primary2, primaryDark2, new PesaToolbar.OnPesaToolbarExpandedListener() {
                        @Override
                        public void onPesaToolbarExpanded() {
                            showFab();
                        }
                    });
                } else {
                    hideFab();
                    pesaToolbar.collapse();
                }
            }
        });

        if (dashboard.getSupportActionBar() != null) {
            dashboard.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP
                    | ActionBar.DISPLAY_SHOW_TITLE
                    | ActionBar.DISPLAY_SHOW_CUSTOM);
            dashboard.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        prepareLayouts();

    }

    private void prepareLayouts() {
        depositDialogLayout();

    }

    private void depositDialogLayout() {
        depositDialog = new MaterialDialog.Builder(dashboard)
                .title("Make a Deposit")
                .backgroundColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_deposit, false)
                .autoDismiss(false)
                .negativeText("Cancel")
                .positiveText("Deposit")
                .positiveColorRes(R.color.colorPrimaryLight)
                .negativeColorRes(R.color.colorPrimary)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        depositDialog.dismiss();
                    }
                })
                .build();
    }

    private void disableAppBarDrag() {
        // see http://stackoverflow.com/questions/34108501/how-to-disable-scrolling-of-appbarlayout-in-coordinatorlayout
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        params.setBehavior(behavior);
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return false;
            }
        });
    }

    /**
     * To hide fab, you need to remove its anchor
     */
    private void hideFab() {
        // Ugly bug makes the view go to bottom|center of screen before hiding, seems like you need to implement your own fab behavior...
        fabPhoto.setVisibility(View.GONE);
        final CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) fabPhoto.getLayoutParams();
        layoutParams.setAnchorId(View.NO_ID);
        fabPhoto.requestLayout();
        fabPhoto.hide();
    }

    private void showFab() {
        final CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) fabPhoto.getLayoutParams();
        layoutParams.setAnchorId(R.id.layout_app_bar);
        layoutParams.anchorGravity = Gravity.RIGHT | Gravity.END | Gravity.BOTTOM;
        fabPhoto.requestLayout();
        fabPhoto.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if (!pesaToolbar.isCollapsed()) {
                    hideFab();
                    pesaToolbar.collapse();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
