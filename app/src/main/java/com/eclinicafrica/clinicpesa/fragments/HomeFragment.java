package com.eclinicafrica.clinicpesa.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.eclinicafrica.clinicpesa.R;
import com.eclinicafrica.clinicpesa.activities.BusinessAccountProfile;
import com.eclinicafrica.clinicpesa.models.HomeScreenItem;

import java.util.List;

import adapters.HomeScreenAdapter;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeFragment extends BaseFragment {

    //    @BindView(R.id.home_recycler_view)
//    RecyclerView recyclerView;
    List<HomeScreenItem> itemList;

    private MaterialDialog depositDialog, paymentDialog, requestExtensionDialog, transferDialog;

    @OnClick(R.id.layout_deposit)
    public void makeDeposit() {

        depositDialog.show();
    }

    @OnClick(R.id.layout_make_payment)
    public void makePayment() {

        paymentDialog.show();
    }

    @OnClick(R.id.layout_extension)
    public void requestExtension() {

        requestExtensionDialog.show();
    }

    @OnClick(R.id.layout_transfer_voucher)
    public void transferVoucher() {

        transferDialog.show();
    }

    @OnClick(R.id.layout_my_account)
    public void onMyAccount() {
//        mListener.onMyAccountSelected(); TODO Change back to this

        startActivity(new Intent(dashboard, BusinessAccountProfile.class));
    }

    private HomeScreenAdapter homeScreenAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        prepareLayouts();
    }

    private void prepareLayouts() {
        depositDialogLayout();

        paymentDialogLayout();

        requestExtensionDialogLayout();

        transferVoucherDialog();


    }

    private void transferVoucherDialog() {
        transferDialog = new MaterialDialog.Builder(dashboard)
                .title("Transfer Voucher")
                .backgroundColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.transfer_voucher_dialog, false)
                .autoDismiss(false)
                .negativeText("Cancel")
                .positiveText("CONFIRM")
                .positiveColorRes(R.color.colorPrimaryLight)
                .negativeColorRes(R.color.colorPrimary)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        transferDialog.dismiss();
                    }
                })
                .build();
    }

    private void requestExtensionDialogLayout() {
        requestExtensionDialog = new MaterialDialog.Builder(dashboard)
                .title("Request Extension")
                .backgroundColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.extension_deposit, false)
                .autoDismiss(false)
                .negativeText("Cancel")
                .positiveText("REQUEST")
                .positiveColorRes(R.color.colorPrimaryLight)
                .negativeColorRes(R.color.colorPrimary)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        requestExtensionDialog.dismiss();
                    }
                })
                .build();
    }

    private void paymentDialogLayout() {
        paymentDialog = new MaterialDialog.Builder(dashboard)
                .title("Make payment")
                .backgroundColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.make_payment_dialog, false)
                .autoDismiss(false)
                .negativeText("Cancel")
                .positiveText("Pay Now")
                .positiveColorRes(R.color.colorPrimaryLight)
                .negativeColorRes(R.color.colorPrimary)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        paymentDialog.dismiss();
                    }
                })
                .build();

    }

    private void depositDialogLayout() {
        depositDialog = new MaterialDialog.Builder(dashboard)
                .title("Make a Deposit")
                .backgroundColorRes(R.color.colorPrimaryDark)
                .customView(R.layout.dialog_deposit, false)
                .autoDismiss(false)
                .negativeText("Cancel")
                .positiveText("Deposit")
                .positiveColorRes(R.color.colorPrimaryLight)
                .negativeColorRes(R.color.colorPrimary)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        depositDialog.dismiss();
                    }
                })
                .build();
    }

}
