package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eclinicafrica.clinicpesa.R;
import com.eclinicafrica.clinicpesa.models.HomeScreenItem;

import java.util.List;

/**
 * Created by bryanlamtoo on 23/04/2017.
 */


public class HomeScreenAdapter extends RecyclerView.Adapter<HomeScreenAdapter.HomeItemViewHolder> {

    private Context mContext;
    private List<HomeScreenItem> homeScreenItemList;


    public HomeScreenAdapter(Context context, List<HomeScreenItem> itemList) {
        this.mContext = context;
        this.homeScreenItemList = itemList;
    }

    @Override
    public HomeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_screen_item, parent, false);

        return new HomeItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeItemViewHolder holder, int position) {
        HomeScreenItem item = homeScreenItemList.get(position);

        holder.title.setText(item.getTitleResId());
        holder.thumbnail.setImageResource(item.getIconResId());
    }

    @Override
    public int getItemCount() {
        return homeScreenItemList.size();
    }

    public class HomeItemViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;

        public HomeItemViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            thumbnail = (ImageView) itemView.findViewById(R.id.icon);
        }
    }
}
